function CanvasApp( canvasId ){
    
    console.log( 'CanvasApp created.' );
    var that = this;
    
    var canvasId = canvasId;
    var canvas = null;
    var context = null;
    
    var clock = new Clock( 200, 600, 300, 'rgba(222,222,222,0.5)' );
    
    function init(){
        console.log( 'CanvasApp: init()' );
        
        canvas = document.getElementById( canvasId );
        context = canvas.getContext( '2d' );
        
        resize();
        window.addEventListener( 'resize', resize );
        
        // start animation loop
        requestAnimationFrame( update );
    }
    
    function resize(){
        console.log( 'CanvasApp: resize()' );
        
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    
    function update(){
        context.clearRect( 0, 0, canvas.width, canvas.height );
        clock.getCurrentTime();
        clock.draw( context );
        requestAnimationFrame( update );
    }
    
    window.addEventListener( 'load', init );
}

function Clock( radius, centerX, centerY, faceColour ){
    
    console.log( 'Clock created.' );
    
    var that = this;
    
    var radius = radius;
    var centerX = centerX;
    var centerY = centerY;
    var faceColour = faceColour;
    
    var hourHand = new HourHand();
    var minuteHand = new MinuteHand();
    var secondHand = new SecondHand();
    
    console.log( secondHand );
    
    const FULL_CIRCLE = Math.PI * 2;

    // setInterval( getCurrentTime, 100 );
    
    this.getCurrentTime = function(){
        var date = new Date();
        
        secondHand.setTime( date.getSeconds(), date.getMilliseconds() );
        minuteHand.setTime( date.getMinutes(), date.getSeconds() );
        hourHand.setTime( date.getHours(), date.getMinutes() );
    }
    
    this.draw = function( context ){
        
        context.save();
        context.translate( centerX, centerY );
        context.fillStyle = faceColour;
        context.beginPath();
        context.arc( 0, 0, radius, 0, FULL_CIRCLE );
        context.closePath();
        context.fill();
        
        // draw the numbers on the clock face
        context.fillStyle = '#ffd214';
        context.font = '40px Helvetica';
        var angle = - ( 1/6 * FULL_CIRCLE );
        var distance = radius * 0.75;
        for( var i = 1; i <= 12; i++ ){
            var x = Math.cos( angle ) * distance - 15;
            var y = Math.sin( angle ) * distance + 15;
            angle += ( 1/12 * FULL_CIRCLE );
            context.fillText( i.toString(), x, y );
        }
        
        hourHand.draw( context );
        minuteHand.draw( context );
        secondHand.draw( context );
        context.restore();
    }
}

function ClockHand(){
    
    console.log( 'ClockHand created.' );
    
    var that = this;
    
    this.thickness = 20;
    this.size = 100;
    this.colour = '#000000';
    this.angle = 0;
    this.x = 0;
    this.y = 0;
    this.timeText = '00';
    
    this.FULL_CIRCLE = Math.PI * 2;
    this.QUARTER_CIRCLE = Math.PI * 0.5;

    this.setTime = function( time ){
        throw new Error( 'This method must be implemented by child objects.' );
    }
    
    this.draw = function( context ){
        context.save();
        context.fillStyle = that.colour;
        context.rotate( that.angle );
        context.fillRect( that.x - (that.thickness * 0.5), 
                          that.y - (that.thickness * 0.5), 
                          that.size, 
                          that.thickness );
        context.restore();
    }
}

function SecondHand(){
    
    console.log( 'SecondHand created.' );
    
    var that = this;
    
    ClockHand.call( this );
    
    this.size = 200;
    this.thickness = 10;
    this.colour = '#ffffff';
    
    this.setTime = function( seconds, milliseconds ){
        that.angle = ((( seconds / 60 ) + ( milliseconds / 60000 )) * that.FULL_CIRCLE ) - that.QUARTER_CIRCLE;
        that.timeText = seconds.toString();
    }
}

SecondHand.prototype = Object.create( ClockHand );

function MinuteHand(){
    
    console.log( 'MinuteHand created.' );
    
    var that = this;
    
    ClockHand.call( this );
    
    this.size = 150;
    
    this.setTime = function( minutes, seconds ){
        that.angle = ( ( ( minutes / 60 ) + ( seconds / 3600 ) ) * that.FULL_CIRCLE ) - that.QUARTER_CIRCLE;
        that.timeText = minutes.toString();
    }
}

MinuteHand.prototype = Object.create( ClockHand );

function HourHand(){
    
    console.log( 'HourHand created.' );
    
    var that = this;
    
    ClockHand.call( this );
    
    this.size = 100;
    
    this.setTime = function( hours, minutes ){
        that.angle = ((( hours / 12 ) + ( minutes / 720 )) 
                      * that.FULL_CIRCLE ) - that.QUARTER_CIRCLE;
        that.timeText = hours.toString();
    }
}
HourHand.prototype = Object.create( ClockHand );

var canvasApp = new CanvasApp( 'drawing-surface' );